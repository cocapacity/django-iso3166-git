import re
from datetime import date

from django.conf import settings
from django.db import models
from django.core.validators import MinLengthValidator, MaxValueValidator, MinValueValidator
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext_lazy as _

from mptt.models import MPTTModel, TreeForeignKey

from .validators import IsAlpha, IsUpperCase
from .managers import *


try:
    default_translation = settings.ISO3166_DEFAUL_LANG
except AttributeError:
    default_translation = "en"

# AA, QM to QZ, XA to XZ, and ZZ
NON_STANDARD_ISO2 = re.compile(r"AA|Q[M-Z]|X[A-Z]|ZZ")

# AAA to AAZ, QMA to QZZ, XAA to XZZ, and ZZA to ZZZ
NON_STANDARD_ISO3 = re.compile(r"AA[A-Z]|Q[M-Z][A-Z]|X[A-Z]{2}|ZZ[A-Z]")


class Country(models.Model):
    """
    Model holding three unique identities per country.

    number
        ISO 3166-1 numeric
    iso2
        ISO 3166-1 alpha-2
    iso3
        ISO 3166-1 alpha-3

    """
    numeric = models.PositiveSmallIntegerField(_("ISO numeric"), db_index=True,
                                               validators=[MinValueValidator(1),
                                                           MaxValueValidator(999)])
    iso2 = models.CharField(_("ISO alpha-2"), max_length=2, db_index=True,
                            validators=[MinLengthValidator(2), IsAlpha, IsUpperCase])
    iso3 = models.CharField(_("ISO alpha-3"), max_length=3, db_index=True,
                            validators=[MinLengthValidator(3), IsAlpha, IsUpperCase])
    # available country selection within time range.
    added_on = models.DateField(_("added on"), db_index=True,
                                default=date(year=1974, month=1, day=1))
    deleted_on = models.DateField(_("deleted on"), null=True, blank=True,
                                  default=None, db_index=True)

    objects = CountryManager()

    class Meta:
        verbose_name = _("country")
        verbose_name_plural = _("countries")
        unique_together = (
            ("numeric", "iso2", "iso3"), # unique set
            ("numeric", "deleted_on"), # unique numeric
            ("iso2", "deleted_on"), # unique iso2
            ("iso3", "deleted_on"), # unique iso3
        )

    def __unicode__(self):
        return u"Country[%d]" % self.numeric

    def natural_key(self):
        return (self.numeric, self.iso2, self.iso3)

    def get_name(self, translation=default_translation):
        """
        Get country name based on translation, at least default language
        should be defined for a country.
        """
        for name in self.names.all():
            if name.translation == translation:
                return name.display_name
        return unicode(self)

    def nonStandard(self):
        return self.numeric >= 900 or \
               NON_STANDARD_ISO2.search(self.iso2) or \
               NON_STANDARD_ISO3.search(self.iso3)


class CountryName(models.Model):
    """
    country
        country
    translation
        settings.LANGUAGES
    display_name
        Display name for a country.
    inverted_name
        Official country name using UPPERCASE.
    """
    country = models.ForeignKey(Country, related_name="names")
    translation = models.CharField(_("language"), max_length=5, db_index=True,
                                   choices=settings.LANGUAGES)
    display_name = models.CharField(_("display name"), max_length=255,
                                    db_index=True,
                                    help_text=mark_safe(_("example: &quot;British Virgin Islands&quot")))
    inverted_name = models.CharField(_("inverted name"), max_length=255,
                                     db_index=True, validators=[IsUpperCase],
                                     help_text=mark_safe(_("Official name, UPPERCASE. example: &quot;VIRGIN ISLANDS, BRITISH&quot;")))

    objects = CountryNameManager()

    class Meta:
        verbose_name = _("country name")
        verbose_name_plural = _("country names")
        unique_together = ("country", "translation")

    def __unicode__(self):
        return u"%s[%s]: %s" % (unicode(self.country), self.translation,
                                self.display_name)

    def natural_key(self):
        return (self.translation, ) + self.country.natural_key()
    natural_key.dependencies = ['iso3166.country']


class Region(MPTTModel):
    """
    Unique name for a list of countries.
    """
    name = models.CharField(_("name of the region"), max_length=255,
                            unique=True)
    parent = TreeForeignKey('self', related_name="subregions", default=None,
                            blank=True, null=True, db_index=True)
    countries = models.ManyToManyField(Country, related_name="regions")

    def __unicode__(self):
        return unicode(self.name)

    def path(self, delimiter=u' :: '):
        """
        Create a name based on hierarchy of the region
        """
        if self.parent:
            return delimiter.join(self.parent.path(delimiter), r.name)
        return r.name
