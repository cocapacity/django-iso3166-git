from django.forms.models import ModelChoiceField
from django.utils.encoding import smart_unicode

__all__ = ('CountryChoiceField',)


class CountryChoiceField(ModelChoiceField):
    """
    Instead of giving back the `pk` it will return the default language name.
    """

    def label_from_instance(self, obj):
        return smart_unicode(obj.get_name())


# South integration
try:
    from south.modelsinspector import add_introspection_rules
    add_introspection_rules([], ["^iso3166\.fields\.CountryChoiceField"])
except ImportError:
    pass
