from django import template

register = template.Library()


@register.filter
def get_countryname(country, translation):
    return country.get_name(translation)
