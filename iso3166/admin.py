from django.contrib import admin
from django.utils.translation import ugettext as _

from iso3166.models import Country, CountryName, Region


class CountryNameInline(admin.TabularInline):
    model = CountryName


class CountryAdmin(admin.ModelAdmin):
    list_display = ("get_name", "numeric", "iso2", "iso3")
    search_fields = ["names__name", "=numeric", "=iso2", "=iso3"]
    inlines = [CountryNameInline]
admin.site.register(Country, CountryAdmin)



def display__name(obj):
    return u"%s%s" % (obj.level * "--", obj.name)
display__name.short_description = _('name')


def display_countries(obj):
    return u", ".join([c.get_name() for c in obj.countries.all()])
display_countries.short_description = _('countries')


def copy_countries_to_parent(modeladmin, request, queryset):
    """
    Copies countries selected in a region to its parent region.

    This so we don't have todo more manual labor then needed,
    this is not automaticly done as we give the freedom to create your
    own hierarchy.
    """
    regions = list(queryset.exclude(parent__isnull=True))

    def inner(region):
        if region.parent in regions:
            inner(region.parent)
            regions.remove(region.parent)
        for c in region.countries.all():
            region.parent.countries.add(c)

    while len(regions) > 0:
        inner(regions.pop())

copy_countries_to_parent.short_description = _("Copy countries to parent region(s).")

class RegionAdmin(admin.ModelAdmin):
    list_display = (display__name, display_countries)
    search_fields = ["name", "countries__names__name", "=countries__numeric",
                     "=countries__iso2", "=countries__iso3"]
    actions = [copy_countries_to_parent]

admin.site.register(Region, RegionAdmin)
