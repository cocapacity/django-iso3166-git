try:
    VERSION = __import__('pkg_resources') \
        .get_distribution('django-iso3166').version
except Exception, e:
    VERSION = 'unknown'