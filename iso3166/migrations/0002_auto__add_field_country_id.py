# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Country.id'
        db.add_column('iso3166_country', 'id',
                      self.gf('django.db.models.fields.IntegerField')(null=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Country.id'
        db.delete_column('iso3166_country', 'id')


    models = {
        'iso3166.country': {
            'Meta': {'object_name': 'Country'},
            'id': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'iso2': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '2'}),
            'iso3': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '3'}),
            'numeric': ('django.db.models.fields.PositiveSmallIntegerField', [], {'primary_key': 'True'})
        },
        'iso3166.countryname': {
            'Meta': {'unique_together': "(('country', 'translation'),)", 'object_name': 'CountryName'},
            'country': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'names'", 'to': "orm['iso3166.Country']"}),
            'display_name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'db_index': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'inverted_name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'db_index': 'True'}),
            'translation': ('django.db.models.fields.CharField', [], {'max_length': '5', 'db_index': 'True'})
        },
        'iso3166.region': {
            'Meta': {'object_name': 'Region'},
            'countries': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'regions'", 'symmetrical': 'False', 'to': "orm['iso3166.Country']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'default': 'None', 'related_name': "'subregions'", 'null': 'True', 'blank': 'True', 'to': "orm['iso3166.Region']"})
        }
    }

    complete_apps = ['iso3166']