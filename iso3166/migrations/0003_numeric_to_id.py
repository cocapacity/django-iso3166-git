# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import DataMigration
from django.db import models

class Migration(DataMigration):

    def forwards(self, orm):
        "Write your forwards methods here."
        orm['iso3166.country'].objects.update(id=models.F('numeric'))

    def backwards(self, orm):
        "Write your backwards methods here."

    models = {
        'iso3166.country': {
            'Meta': {'object_name': 'Country'},
            'id': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'iso2': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '2'}),
            'iso3': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '3'}),
            'numeric': ('django.db.models.fields.PositiveSmallIntegerField', [], {'primary_key': 'True'})
        },
        'iso3166.countryname': {
            'Meta': {'unique_together': "(('country', 'translation'),)", 'object_name': 'CountryName'},
            'country': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'names'", 'to': "orm['iso3166.Country']"}),
            'display_name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'db_index': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'inverted_name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'db_index': 'True'}),
            'translation': ('django.db.models.fields.CharField', [], {'max_length': '5', 'db_index': 'True'})
        },
        'iso3166.region': {
            'Meta': {'object_name': 'Region'},
            'countries': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'regions'", 'symmetrical': 'False', 'to': "orm['iso3166.Country']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'default': 'None', 'related_name': "'subregions'", 'null': 'True', 'blank': 'True', 'to': "orm['iso3166.Region']"})
        }
    }

    complete_apps = ['iso3166']
    symmetrical = True
