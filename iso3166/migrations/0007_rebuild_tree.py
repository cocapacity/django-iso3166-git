# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import DataMigration
from django.db import models

class Migration(DataMigration):

    def forwards(self, orm):
        "Write your forwards methods here."
        orm['iso3166.region'].objects.rebuild()

    def backwards(self, orm):
        "Write your backwards methods here."

    models = {
        'iso3166.country': {
            'Meta': {'unique_together': "(('numeric', 'iso2', 'iso3'), ('numeric', 'deleted_on'), ('iso2', 'deleted_on'), ('iso3', 'deleted_on'))", 'object_name': 'Country'},
            'added_on': ('django.db.models.fields.DateField', [], {'default': 'datetime.datetime(1974, 1, 1, 0, 0)', 'db_index': 'True'}),
            'deleted_on': ('django.db.models.fields.DateField', [], {'default': 'None', 'null': 'True', 'db_index': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'iso2': ('django.db.models.fields.CharField', [], {'max_length': '2', 'db_index': 'True'}),
            'iso3': ('django.db.models.fields.CharField', [], {'max_length': '3', 'db_index': 'True'}),
            'numeric': ('django.db.models.fields.PositiveSmallIntegerField', [], {'db_index': 'True'})
        },
        'iso3166.countryname': {
            'Meta': {'unique_together': "(('country', 'translation'),)", 'object_name': 'CountryName'},
            'country': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'names'", 'to': "orm['iso3166.Country']"}),
            'display_name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'db_index': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'inverted_name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'db_index': 'True'}),
            'translation': ('django.db.models.fields.CharField', [], {'max_length': '5', 'db_index': 'True'})
        },
        'iso3166.region': {
            'Meta': {'object_name': 'Region', '_bases': ('mptt.models.MPTTModel',)},
            'countries': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'regions'", 'symmetrical': 'False', 'to': "orm['iso3166.Country']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'level': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'lft': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'}),
            'parent': ('mptt.fields.TreeForeignKey', [], {'default': 'None', 'related_name': "'subregions'", 'null': 'True', 'blank': 'True', 'to': "orm['iso3166.Region']"}),
            'rght': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'tree_id': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'})
        }
    }

    complete_apps = ['iso3166']
    symmetrical = True
