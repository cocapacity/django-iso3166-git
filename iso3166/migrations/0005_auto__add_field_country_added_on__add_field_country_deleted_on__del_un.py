# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Removing unique constraint on 'Country', fields ['numeric']
        db.delete_unique('iso3166_country', ['numeric'])

        # Removing unique constraint on 'Country', fields ['iso2']
        db.delete_unique('iso3166_country', ['iso2'])

        # Removing unique constraint on 'Country', fields ['iso3']
        db.delete_unique('iso3166_country', ['iso3'])

        # Adding field 'Country.added_on'
        db.add_column('iso3166_country', 'added_on',
                      self.gf('django.db.models.fields.DateField')(default=datetime.datetime(1974, 1, 1, 0, 0), db_index=True),
                      keep_default=False)

        # Adding field 'Country.deleted_on'
        db.add_column('iso3166_country', 'deleted_on',
                      self.gf('django.db.models.fields.DateField')(default=None, null=True, db_index=True, blank=True),
                      keep_default=False)

        # Adding index on 'Country', fields ['iso3']
        db.create_index('iso3166_country', ['iso3'])

        # Adding index on 'Country', fields ['iso2']
        db.create_index('iso3166_country', ['iso2'])

        # Adding index on 'Country', fields ['numeric']
        db.create_index('iso3166_country', ['numeric'])

        # Adding unique constraint on 'Country', fields ['iso3', 'iso2', 'numeric']
        db.create_unique('iso3166_country', ['iso3', 'iso2', 'numeric'])

        # Adding unique constraint on 'Country', fields ['numeric', 'deleted_on']
        db.create_unique('iso3166_country', ['numeric', 'deleted_on'])

        # Adding unique constraint on 'Country', fields ['iso2', 'deleted_on']
        db.create_unique('iso3166_country', ['iso2', 'deleted_on'])

        # Adding unique constraint on 'Country', fields ['iso3', 'deleted_on']
        db.create_unique('iso3166_country', ['iso3', 'deleted_on'])


    def backwards(self, orm):
        # Removing unique constraint on 'Country', fields ['iso3', 'deleted_on']
        db.delete_unique('iso3166_country', ['iso3', 'deleted_on'])

        # Removing unique constraint on 'Country', fields ['iso2', 'deleted_on']
        db.delete_unique('iso3166_country', ['iso2', 'deleted_on'])

        # Removing unique constraint on 'Country', fields ['numeric', 'deleted_on']
        db.delete_unique('iso3166_country', ['numeric', 'deleted_on'])

        # Removing unique constraint on 'Country', fields ['iso3', 'iso2', 'numeric']
        db.delete_unique('iso3166_country', ['iso3', 'iso2', 'numeric'])

        # Removing index on 'Country', fields ['numeric']
        db.delete_index('iso3166_country', ['numeric'])

        # Removing index on 'Country', fields ['iso2']
        db.delete_index('iso3166_country', ['iso2'])

        # Removing index on 'Country', fields ['iso3']
        db.delete_index('iso3166_country', ['iso3'])

        # Deleting field 'Country.added_on'
        db.delete_column('iso3166_country', 'added_on')

        # Deleting field 'Country.deleted_on'
        db.delete_column('iso3166_country', 'deleted_on')

        # Adding unique constraint on 'Country', fields ['iso3']
        db.create_unique('iso3166_country', ['iso3'])

        # Adding unique constraint on 'Country', fields ['iso2']
        db.create_unique('iso3166_country', ['iso2'])

        # Adding unique constraint on 'Country', fields ['numeric']
        db.create_unique('iso3166_country', ['numeric'])


    models = {
        'iso3166.country': {
            'Meta': {'unique_together': "(('numeric', 'iso2', 'iso3'), ('numeric', 'deleted_on'), ('iso2', 'deleted_on'), ('iso3', 'deleted_on'))", 'object_name': 'Country'},
            'added_on': ('django.db.models.fields.DateField', [], {'default': 'datetime.datetime(1974, 1, 1, 0, 0)', 'db_index': 'True'}),
            'deleted_on': ('django.db.models.fields.DateField', [], {'default': 'None', 'null': 'True', 'db_index': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'iso2': ('django.db.models.fields.CharField', [], {'max_length': '2', 'db_index': 'True'}),
            'iso3': ('django.db.models.fields.CharField', [], {'max_length': '3', 'db_index': 'True'}),
            'numeric': ('django.db.models.fields.PositiveSmallIntegerField', [], {'db_index': 'True'})
        },
        'iso3166.countryname': {
            'Meta': {'unique_together': "(('country', 'translation'),)", 'object_name': 'CountryName'},
            'country': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'names'", 'to': "orm['iso3166.Country']"}),
            'display_name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'db_index': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'inverted_name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'db_index': 'True'}),
            'translation': ('django.db.models.fields.CharField', [], {'max_length': '5', 'db_index': 'True'})
        },
        'iso3166.region': {
            'Meta': {'object_name': 'Region'},
            'countries': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'regions'", 'symmetrical': 'False', 'to': "orm['iso3166.Country']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'default': 'None', 'related_name': "'subregions'", 'null': 'True', 'blank': 'True', 'to': "orm['iso3166.Region']"})
        }
    }

    complete_apps = ['iso3166']