# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Region.lft'
        db.add_column('iso3166_region', 'lft',
                      self.gf('django.db.models.fields.PositiveIntegerField')(default=0, db_index=True),
                      keep_default=False)

        # Adding field 'Region.rght'
        db.add_column('iso3166_region', 'rght',
                      self.gf('django.db.models.fields.PositiveIntegerField')(default=0, db_index=True),
                      keep_default=False)

        # Adding field 'Region.tree_id'
        db.add_column('iso3166_region', 'tree_id',
                      self.gf('django.db.models.fields.PositiveIntegerField')(default=0, db_index=True),
                      keep_default=False)

        # Adding field 'Region.level'
        db.add_column('iso3166_region', 'level',
                      self.gf('django.db.models.fields.PositiveIntegerField')(default=0, db_index=True),
                      keep_default=False)


        # Changing field 'Region.parent'
        db.alter_column('iso3166_region', 'parent_id', self.gf('mptt.fields.TreeForeignKey')(null=True, to=orm['iso3166.Region']))


    def backwards(self, orm):
        # Deleting field 'Region.lft'
        db.delete_column('iso3166_region', 'lft')

        # Deleting field 'Region.rght'
        db.delete_column('iso3166_region', 'rght')

        # Deleting field 'Region.tree_id'
        db.delete_column('iso3166_region', 'tree_id')

        # Deleting field 'Region.level'
        db.delete_column('iso3166_region', 'level')


        # Changing field 'Region.parent'
        db.alter_column('iso3166_region', 'parent_id', self.gf('django.db.models.fields.related.ForeignKey')(null=True, to=orm['iso3166.Region']))

    models = {
        'iso3166.country': {
            'Meta': {'unique_together': "(('numeric', 'iso2', 'iso3'), ('numeric', 'deleted_on'), ('iso2', 'deleted_on'), ('iso3', 'deleted_on'))", 'object_name': 'Country'},
            'added_on': ('django.db.models.fields.DateField', [], {'default': 'datetime.datetime(1974, 1, 1, 0, 0)', 'db_index': 'True'}),
            'deleted_on': ('django.db.models.fields.DateField', [], {'default': 'None', 'null': 'True', 'db_index': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'iso2': ('django.db.models.fields.CharField', [], {'max_length': '2', 'db_index': 'True'}),
            'iso3': ('django.db.models.fields.CharField', [], {'max_length': '3', 'db_index': 'True'}),
            'numeric': ('django.db.models.fields.PositiveSmallIntegerField', [], {'db_index': 'True'})
        },
        'iso3166.countryname': {
            'Meta': {'unique_together': "(('country', 'translation'),)", 'object_name': 'CountryName'},
            'country': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'names'", 'to': "orm['iso3166.Country']"}),
            'display_name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'db_index': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'inverted_name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'db_index': 'True'}),
            'translation': ('django.db.models.fields.CharField', [], {'max_length': '5', 'db_index': 'True'})
        },
        'iso3166.region': {
            'Meta': {'object_name': 'Region'},
            'countries': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'regions'", 'symmetrical': 'False', 'to': "orm['iso3166.Country']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'level': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'lft': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'}),
            'parent': ('mptt.fields.TreeForeignKey', [], {'default': 'None', 'related_name': "'subregions'", 'null': 'True', 'blank': 'True', 'to': "orm['iso3166.Region']"}),
            'rght': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'tree_id': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'})
        }
    }

    complete_apps = ['iso3166']