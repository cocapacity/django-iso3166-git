# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Country'
        db.create_table('iso3166_country', (
            ('numeric', self.gf('django.db.models.fields.PositiveSmallIntegerField')(primary_key=True)),
            ('iso2', self.gf('django.db.models.fields.CharField')(unique=True, max_length=2)),
            ('iso3', self.gf('django.db.models.fields.CharField')(unique=True, max_length=3)),
        ))
        db.send_create_signal('iso3166', ['Country'])

        # Adding model 'CountryName'
        db.create_table('iso3166_countryname', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('country', self.gf('django.db.models.fields.related.ForeignKey')(related_name='names', to=orm['iso3166.Country'])),
            ('translation', self.gf('django.db.models.fields.CharField')(max_length=5, db_index=True)),
            ('display_name', self.gf('django.db.models.fields.CharField')(max_length=255, db_index=True)),
            ('inverted_name', self.gf('django.db.models.fields.CharField')(max_length=255, db_index=True)),
        ))
        db.send_create_signal('iso3166', ['CountryName'])

        # Adding unique constraint on 'CountryName', fields ['country', 'translation']
        db.create_unique('iso3166_countryname', ['country_id', 'translation'])

        # Adding model 'Region'
        db.create_table('iso3166_region', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('parent', self.gf('django.db.models.fields.related.ForeignKey')(default=None, related_name='subregions', null=True, blank=True, to=orm['iso3166.Region'])),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=255)),
        ))
        db.send_create_signal('iso3166', ['Region'])

        # Adding M2M table for field countries on 'Region'
        db.create_table('iso3166_region_countries', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('region', models.ForeignKey(orm['iso3166.region'], null=False)),
            ('country', models.ForeignKey(orm['iso3166.country'], null=False))
        ))
        db.create_unique('iso3166_region_countries', ['region_id', 'country_id'])


    def backwards(self, orm):
        # Removing unique constraint on 'CountryName', fields ['country', 'translation']
        db.delete_unique('iso3166_countryname', ['country_id', 'translation'])

        # Deleting model 'Country'
        db.delete_table('iso3166_country')

        # Deleting model 'CountryName'
        db.delete_table('iso3166_countryname')

        # Deleting model 'Region'
        db.delete_table('iso3166_region')

        # Removing M2M table for field countries on 'Region'
        db.delete_table('iso3166_region_countries')


    models = {
        'iso3166.country': {
            'Meta': {'object_name': 'Country'},
            'iso2': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '2'}),
            'iso3': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '3'}),
            'numeric': ('django.db.models.fields.PositiveSmallIntegerField', [], {'primary_key': 'True'})
        },
        'iso3166.countryname': {
            'Meta': {'unique_together': "(('country', 'translation'),)", 'object_name': 'CountryName'},
            'country': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'names'", 'to': "orm['iso3166.Country']"}),
            'display_name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'db_index': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'inverted_name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'db_index': 'True'}),
            'translation': ('django.db.models.fields.CharField', [], {'max_length': '5', 'db_index': 'True'})
        },
        'iso3166.region': {
            'Meta': {'object_name': 'Region'},
            'countries': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'regions'", 'symmetrical': 'False', 'to': "orm['iso3166.Country']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'default': 'None', 'related_name': "'subregions'", 'null': 'True', 'blank': 'True', 'to': "orm['iso3166.Region']"})
        }
    }

    complete_apps = ['iso3166']