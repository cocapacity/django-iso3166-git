from django.core.exceptions import ValidationError


def IsUpperCase(value):
    if value != value.upper():
        raise ValidationError(u'%s contains lowercase characters' % value)
    
    
def IsAlpha(value):
    if not value.isalpha():
        raise ValidationError(u'%s contains non alphabetic characters' % value)
    