from django.db.models import Manager


class CountryManager(Manager):
    """

    """
    def get_query_set(self):
        """
        prefetch the names for the entire query set.
        """
        return super(CountryManager, self).get_query_set().prefetch_related("names")

    def get_by_natural_key(self, numeric, iso2, iso3):
        return self.get(numeric=numeric, iso2=iso2, iso3=iso3)

    def get_for_period(self, start, end):
        """

        """
        return self.get_query_set().filter(added_on__lte=end,
                                           deleted_on__lte=start)


class CountryNameManager(Manager):
    """

    """
    def get_by_natural_key(self, translation, numeric, iso2, iso3):
        return self.get(translation=translation,
                        country_numeric=numeric,
                        country_iso2=iso2,
                        country_iso3=iso3)
