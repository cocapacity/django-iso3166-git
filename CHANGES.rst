========================
django-iso3166 changelog
========================

Version 13.01
-------------
* support django 1.4
* new package layout
* numeric is no longer primarykey
* South support
* natural_key support
* region model supports mptt
* date fields added for new countries or old codes


Version 10.1, 26 Jan 2010
-------------------------
Packaged from revision 105 in Subversion; download at
http://files.co-capacity.biz/iso3166/releases/django-iso3166-10.1.tar.gz

* First packaged version using distutils.
